# About the script

Script loads the CSV and outputs all drawdowns larger than a given value (in percent of the previous maximum), sorted according to the magnitude beginning from the largest.

File "table.csv" contains daily data of index S&P since 1950. Column "Close" is used as the price.

Drawdown is a function of time representing the difference between historical maximum and the current price.

[What is drawdown](https://en.wikipedia.org/wiki/Drawdown_(economics))

Basically script searches for all the maximum drawdowns that do not overlap. You can see the output for 30%:

![picture](img/drawdowns00.png)

For each discovered drawdown, script creates a figure containing a subsection of S&P index price evolution from the drawdown period with traded volumes
as a bar chart:

![picture](img/drawdowns1.png)

![picture](img/drawdowns2.png)

# Run the script

Script uses these packages:
python 3.6, numpy 1.15.2, pandas 0.23.4

Run the script:
`python script.py`
