# ----- change input value -----
input_val = 30.

# ----- rest of the code ------
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

DD_list = []
dataset = pd.read_csv('./table.csv')

# function to compute drawdowns recursively
def find_drawdown_recurs(data_arr):

    if len(data_arr) < 3:
        return None

    run_max = np.maximum.accumulate(data_arr)
    end_per = np.argmax(run_max - data_arr)
    start_per = np.argmax(data_arr[:end_per])

    size_DD = data_arr[start_per] - data_arr[end_per]
    size_DD_perc = size_DD / data_arr[start_per] * 100

    result = {'start': start_per, 'end': end_per, 'size': round(size_DD_perc,2)}

    if size_DD_perc > input_val:
        DD_list.append(result)

        left_res = find_drawdown_recurs(data_arr[0:start_per])
        righ_res = find_drawdown_recurs(data_arr[end_per:len(data_arr)])

        if left_res is not None:
            DD_list.append(left_res)
        if righ_res is not None:
            DD_list.append(righ_res)
    else:
        return None

# compute results
dataset['Date'] = pd.to_datetime(dataset['Date'])
dataset = dataset.sort_values(by='Date').reset_index(drop=True)
data_arr = np.array(dataset['Close'])

# compute maximum drawdowns
find_drawdown_recurs(data_arr)
output_DD_list = sorted(DD_list, key=lambda k: k['size'], reverse=True)

# draw results to one plot
fig1, ax1 = plt.subplots(figsize=(8, 4))
ax1 = dataset.plot(x="Date", y="Close", kind="line", figsize=(20, 5))

points = []
for idx, val in enumerate(output_DD_list):
    subset = dataset.iloc[val['start']:val['end']]
    plt.plot(subset['Date'].values, subset['Close'].values, color='red')

ax1.set_ylabel('Close price')
ax1.legend_.remove()
fig1 = plt.gcf()
fig1.savefig('set_val=' + str(input_val) + '_id=' + str(0) + '.png')

pass

print('Value was set to: ' + str(input_val) + '%')

# draw each drawdown separately in one figure
for idx, val in enumerate(output_DD_list):
    print('Drawdown n.' + str(idx+1) + str(val))

    subset = dataset.iloc[val['start']:val['end']]
    fig, ax1 = plt.subplots(figsize=(8,4))

    # draw to plots, set properties
    ax1.plot(subset['Date'].values,subset['Close'].values, linewidth=3.0, color='red', alpha=.5)
    ax2 = ax1.twinx()
    ax2.bar(subset['Date'].values,subset['Volume'].values, alpha=.3, log=True)
    ax1.set_xlabel('Date')
    ax1.set_ylabel('Close price')
    ax1.yaxis.label.set_color('red')
    ax2.set_ylabel('Volume')
    ax2.yaxis.label.set_color('cornflowerblue')
    ax2.tick_params(axis='y', colors='cornflowerblue', which='both')
    ax1.tick_params(axis='y', colors='red')

    fig.autofmt_xdate()

    fig.savefig('set_val=' + str(input_val) + '_id=' + str(idx+1) + '.png')

print('Finished')